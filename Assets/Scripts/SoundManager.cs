﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    AudioSource audioSource;
    public AudioClip[] audioClips;
    public enum Sound
    {
        Background,
        Jump,
        Incorrect,
        Correct,
        Star,
        Wipe,
        Victory,
        Menu,
        Type1,
        Type2,
        Type3,
    }


    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.loop = true;
    }

    public void playSound(Sound sound)
    {
        switch (sound)
        {
            case Sound.Background:
                audioSource.Stop();
                audioSource.clip = audioClips[0];
                audioSource.Play();
                break;
            case Sound.Jump:
                audioSource.PlayOneShot(audioClips[1]);
                break;
            case Sound.Incorrect:
                audioSource.PlayOneShot(audioClips[2]);
                break;
            case Sound.Correct:
                audioSource.PlayOneShot(audioClips[3]);
                break;
            case Sound.Star:
                audioSource.PlayOneShot(audioClips[4]);
                break;
            case Sound.Wipe:
                audioSource.PlayOneShot(audioClips[5]);
                break;
            case Sound.Victory:
                audioSource.Stop();
                audioSource.PlayOneShot(audioClips[6]);
                break;
            case Sound.Menu:
                audioSource.PlayOneShot(audioClips[7]);
                break;
            case Sound.Type1:
                int randInt = Random.Range(8, 10);
                audioSource.PlayOneShot(audioClips[randInt]);
                break;
        }
    }
}
