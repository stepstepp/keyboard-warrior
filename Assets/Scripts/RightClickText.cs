﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RightClickText : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("ToggleVisible", .5f, 1);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void ToggleVisible()
    {
        gameObject.GetComponent<Text>().color = gameObject.GetComponent<Text>().color == Color.clear ? new Color(50, 50, 50, 255) : Color.clear;
    }
}
