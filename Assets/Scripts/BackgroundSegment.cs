﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundSegment : MonoBehaviour
{
    Material material;
    Vector2 offset;

    public int xVel, yVel;

    void Start()
    {
        material = GetComponent<Renderer>().material;
        offset = new Vector2(xVel, yVel);
    }

    // Update is called once per frame
    void Update()
    {
        if (!PauseMenu.paused)
        {
            offset = new Vector2(xVel, yVel);
            material.mainTextureOffset += offset * Time.unscaledDeltaTime / 20;
        }
    }
}
