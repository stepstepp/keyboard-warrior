﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelButton : MonoBehaviour
{
    public int level;
    private int numStars;

    public GameObject[] levelCompleteStars;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < levelCompleteStars.Length; i++)
        {
            levelCompleteStars[i].SetActive(false);
        }

        numStars = PlayerPrefs.GetInt("Level " + level);

        for (int i = 0; i < numStars; i++)
        {
            levelCompleteStars[i].SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
