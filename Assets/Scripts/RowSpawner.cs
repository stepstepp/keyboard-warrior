﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RowSpawner : MonoBehaviour
{
    public GameObject[] Row;
    public GameObject start;
    public float offset;

    // Start is called before the first frame update
    void Start()
    {
        KeyManager keyManager = GameObject.FindGameObjectWithTag("InputManager").GetComponent<KeyManager>();
        if (Row != null && start != null)
        {
            for (int i = 0; i < Row.Length; i++)
            {
                GameObject key = Instantiate(Row[i], new Vector3(start.transform.position.x + (2.5f * (i + 1)) + offset, transform.position.y, 0), Quaternion.identity, transform);
                keyManager.keys.Add(key);
            }
        }
    }
}
