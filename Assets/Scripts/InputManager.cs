﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using System.Linq;
using System.IO;

public class InputManager : MonoBehaviour
{
    public KeyManager keyManager;
    private InputField inputField;
    private GameObject textField;
    private GameObject numberRow;
    private Color originalColor;
    private GameObject button;
    private Camera cam;
    private String input;
    private String prevWord;

    private int length;

    private Key[] numbers;

    String[] everyWord;
    public bool inputActive;

    private SoundManager soundManager;

    // Start is called before the first frame update
    void Start()
    {
        inputActive = false;

        keyManager.TurnOffAllKeys();

        soundManager = GameObject.Find("SoundManager").GetComponent<SoundManager>();
        inputField = GameObject.FindGameObjectWithTag("InputField").GetComponent<InputField>();
        textField = inputField.transform.GetChild(0).gameObject;
        numberRow = GameObject.FindGameObjectWithTag("Numbers");
        button = GameObject.FindGameObjectWithTag("CheckButton");
        cam = Camera.main;

        numbers = new Key[numberRow.transform.childCount];

        for (int i = 0; i < numberRow.transform.childCount; i++)
        {
            numbers[i] = numberRow.transform.GetChild(i).GetComponent<Key>();
        }

        inputField.characterLimit = 10;
        inputField.contentType = InputField.ContentType.Standard;
        originalColor = inputField.GetComponent<Image>().color;

        inputField.onValueChanged.AddListener(delegate { ValueChangeCheck(); });
        StreamReader sr = new StreamReader(Application.streamingAssetsPath + "/words_alpha.txt");
        var fileContents = sr.ReadToEnd();
        sr.Close();
        everyWord = fileContents.Split("\n"[0]);
    }

    // Update is called once per frame
    void Update()
    {
        //DEVHACKS
        if (Input.GetKey(KeyCode.F1))
        {
            if (Input.GetKeyDown(KeyCode.Equals))
            {
                foreach (char c in "asdfghjkl")
                {
                    char thisChar;
                    thisChar = char.ToUpper(c);

                    keyManager.TurnOnKey(thisChar.ToString());
                }
            }
            if (Input.GetKeyDown(KeyCode.Minus))
            {
                foreach (char c in "uxmdefqzs")
                {
                    char thisChar;
                    thisChar = char.ToUpper(c);

                    keyManager.TurnOnKey(thisChar.ToString());
                }
            }
        }
        if (Input.GetKey(KeyCode.F2))
        {
            string inputString = Input.inputString.ToUpper();
            char[] inputChar = inputString.ToCharArray();
            foreach (char c in inputChar)
            {
                char thisChar;
                thisChar = char.ToUpper(c);

                keyManager.TurnOnKey(thisChar.ToString());
            }
        }

        if (Input.GetKeyDown(KeyCode.F3))
        {
            keyManager.TurnOffAllKeys();
        }


        if (textField.GetComponent<Text>().text != null)
        {
            length = textField.GetComponent<Text>().text.Length;
        }

        if (inputActive && Input.GetKeyDown(KeyCode.Return))
        {
            ReadInput();
        }

        if (inputActive)
        {
            numberRow.SetActive(true);
            inputField.gameObject.SetActive(true);
            button.SetActive(true);

            cam.GetComponent<CameraFollow>().followCam = false;

            ChangeNumberKeys(0);

            for (int i = 0; i < length; i++)
            {
                numbers[9 - i].keyState = 1;
            }
        }
        else
        {
            numberRow.SetActive(false);
            inputField.gameObject.SetActive(false);
            button.SetActive(false);

            ChangeNumberKeys(1);

            if (cam.GetComponent<CameraFollow>().followCam == false)
            {
                cam.transform.position = cam.GetComponent<CameraFollow>().target.position;
            }

            cam.GetComponent<CameraFollow>().followCam = true;
        }
    }


    public void ReadInput()
    {
        keyManager.TurnOffAllKeys();
        input = textField.GetComponent<Text>().text;

        bool found = false;

        foreach (string thisword in everyWord)
        {
            bool hasRed = false;

            if (input == thisword.ToLower().TrimEnd().TrimStart())
            {
                foreach (char c in input)
                {
                    char thisChar;
                    thisChar = char.ToUpper(c);

                    if (keyManager.CheckRed(thisChar.ToString()))
                    {
                        hasRed = true;
                    }
                }
                if (!hasRed)
                    found = true;
            }
        }

        if (found)
        {
            soundManager.playSound(SoundManager.Sound.Correct);

            Debug.Log(input);

            foreach (char c in input)
            {
                char thisChar;
                thisChar = char.ToUpper(c);

                keyManager.TurnOnKey(thisChar.ToString());
            }
            prevWord = input;
            inputField.GetComponent<Image>().color = new Color(0, 0.5f, 0, 1);
            Invoke("ResetColor", 0.5f);
            Invoke("ToggleInputActive", 1f);
        }
        else
        {
            soundManager.playSound(SoundManager.Sound.Incorrect);

            if (prevWord != null)
            {
                foreach (char c in prevWord)
                {
                    char thisChar;
                    thisChar = char.ToUpper(c);

                    keyManager.TurnOnKey(thisChar.ToString());
                }

            }
            inputField.GetComponent<Image>().color = new Color(.5f, 0, 0, 1);
            Invoke("ResetColor", 0.5f);
        }

    }
    private void ValueChangeCheck()
    {
        soundManager.playSound(SoundManager.Sound.Type1);
    }

    private void ChangeNumberKeys(int state)
    {
        for (int i = 0; i < inputField.characterLimit; i++)
        {
            numbers[i].keyState = state;
        }
    }

    void ResetColor()
    {
        inputField.GetComponent<Image>().color = originalColor;
    }

    public void ToggleInputActive()
    {
        inputActive = inputActive == false ? true : false;
    }

    public void ClearInput()
    {
        inputField.SetTextWithoutNotify("");
        keyManager.TurnOffAllKeys();
    }
}
