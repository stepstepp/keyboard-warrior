﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyManager : MonoBehaviour
{
    public List<GameObject> keys;

    // Start is called before the first frame update
    void Start()
    {
        foreach (GameObject key in GameObject.FindGameObjectsWithTag("Letter"))
        {
            keys.Add(key);
        }
    }

    public void TurnOffKey(string key)
    {
        foreach (GameObject k in keys)
        {
            Key thisKey = k.GetComponent<Key>();

            if (thisKey.keyState == 0)
            {
                if (thisKey.GetLetter() == key)
                {
                    thisKey.keyState = 1;
                }
            }
        }
    }

    public void TurnOnKey(string key)
    {
        foreach (GameObject k in keys)
        {
            Key thisKey = k.GetComponent<Key>();

            {
                if (thisKey.GetLetter() == key)
                {
                    thisKey.keyState = 0;
                }
            }

        }
    }

    public void TurnOffAllKeys()
    {
        foreach (GameObject k in keys)
        {
            Key thisKey = k.GetComponent<Key>();

            thisKey.keyState = 1;

        }
    }

    public bool CheckRed(string key)
    {
        foreach (GameObject k in keys)
        {
            Key thisKey = k.GetComponent<Key>();

            if (thisKey.GetLetter() == key)
            {
                if (thisKey.isRed)
                {
                    return true;
                }
            }
        }
        return false;
    }
}
