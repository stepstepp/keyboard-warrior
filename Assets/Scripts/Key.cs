﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour
{
    private GameObject onState;
    private GameObject offState;
    private GameObject redState;
    public bool keyOn;
    public int keyState = 1;

    public bool isRed;

    public enum Letter
    {
        A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z, BLANK
    }

    public Letter letter;

    // Start is called before the first frame update
    void Start()
    {
        onState = gameObject.transform.GetChild(0).gameObject;

        offState = gameObject.transform.GetChild(1).gameObject;

        redState = gameObject.transform.GetChild(2).gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (keyState == 0 || keyOn) //on
        {
            onState.SetActive(true);
            offState.SetActive(false);
            redState.SetActive(false);
        }
        else if (keyState == 1) //off
        {
            onState.SetActive(false);
            offState.SetActive(true);
            redState.SetActive(false);
            offState.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, .3f);
        }
        else //red
        {
            onState.SetActive(false);
            offState.SetActive(false);
            redState.SetActive(true);
        }

        if (isRed)
        {
            keyState = 2;
        }
    }

    public string GetLetter()
    {
        return gameObject.GetComponent<Key>().letter.ToString();
    }
}
