﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject levelCompleteMenuUI;
    public GameObject collectedStarsText;
    public Text Timer;
    private GameObject Fade;
    private int numStars;

    private GameObject[] stars;
    private GameObject[] levelCompleteStars;

    private SoundManager soundManager;

    private TimeSpan timePlaying;
    private float elapsedTime;

    // Start is called before the first frame update
    void Start()
    {
        elapsedTime = 0;

        Fade = GameObject.FindGameObjectWithTag("Fade");
        Time.timeScale = 1f;
        soundManager = GameObject.Find("SoundManager").GetComponent<SoundManager>();
        soundManager.playSound(SoundManager.Sound.Background);

        stars = GameObject.FindGameObjectsWithTag("Star");
        levelCompleteStars = GameObject.FindGameObjectsWithTag("LevelCompleteStar");
        foreach (GameObject star in levelCompleteStars)
        {
            star.transform.parent.gameObject.SetActive(false);
        }

        numStars = 0;
        levelCompleteMenuUI.SetActive(false);
        Fade.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        elapsedTime += Time.deltaTime;
        timePlaying = TimeSpan.FromSeconds(elapsedTime);

        //minuteCount = (int)(Time.time / 60f);
        //secondsCount = (int)(Time.time % 60f);
        Timer.text = "Time: "  +  timePlaying.ToString("mm':'ss':'ff");


        collectedStarsText.GetComponent<Text>().text = "Collected Stars: " + numStars;
    }
    public void LevelComplete()
    {

        if (PlayerPrefs.GetInt(SceneManager.GetActiveScene().name) < numStars)
        {
            PlayerPrefs.SetInt(SceneManager.GetActiveScene().name, numStars);
        }

        Time.timeScale = 0f;
        PauseMenu.canPause = false;
        collectedStarsText.SetActive(false);
        Timer.gameObject.SetActive(false);
        levelCompleteMenuUI.SetActive(true);

        Text finalTimerText = GameObject.FindGameObjectWithTag("FinalTimerText").GetComponent<Text>();
        finalTimerText.text = "in " + timePlaying.ToString("mm':'ss':'ff") + "!";


        for (int i = 0; i < numStars; i++)
        {
            levelCompleteStars[i].transform.parent.gameObject.SetActive(true);
        }
    }

    public void CollectStar(Collider2D star)
    {
        star.transform.parent.gameObject.SetActive(false);
        numStars++;
    }

    public void VoidOut()
    {
        numStars = 0;
        Fade.SetActive(true);

        Fade.GetComponent<Animator>().SetTrigger("screenBlack");
        Invoke("FadeActive", 1.2f);

        foreach (GameObject star in stars)
        {
            star.transform.parent.gameObject.SetActive(true);
        }
    }

    private void FadeActive()
    {
        Fade.SetActive(false);
    }
}
