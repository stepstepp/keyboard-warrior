﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public bool canMove;
    public bool canPress;

    public float speed;
    public float jumpForce;
    public float fallMultiplier = 2.5f;
    private float moveInput;

    private Vector3 startPos;
    private Rigidbody2D rb;

    private bool facingRight = true;

    private bool isGrounded;
    private bool canDoubleJump;
    public Transform groundCheck;
    public LayerMask whatIsGround;
    public float checkRadius;

    private Animator animator;

    private InputManager inputManager;
    private Camera cam;

    private GameManager gameManager;
    private SoundManager soundManager;

    private GameObject typeButton;
    public Sprite unPressed;
    public Sprite pressed;

    // Start is called before the first frame update
    void Start()
    {
        typeButton = GameObject.FindGameObjectWithTag("TypeButton");
        typeButton.SetActive(false);
        soundManager = GameObject.Find("SoundManager").GetComponent<SoundManager>();

        PauseMenu.canPause = true;
        canMove = true;
        canPress = true;
        inputManager = GameObject.FindGameObjectWithTag("InputManager").GetComponent<InputManager>();
        gameManager = GameObject.Find(("GameManager")).GetComponent<GameManager>();

        cam = Camera.main;
        rb = GetComponent<Rigidbody2D>();
        animator = gameObject.GetComponent<Animator>();
        startPos = gameObject.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        //DEVHACK
        if (Input.GetKeyDown(KeyCode.F4))
        {
            transform.GetComponent<Rigidbody2D>().isKinematic = transform.GetComponent<Rigidbody2D>().isKinematic == false ? true : false;
        }


        UpdateInput();

        canMove = inputManager.inputActive == false ? true : false;

        if (Input.GetButtonDown("Jump") && canMove)
        {
            soundManager.playSound(SoundManager.Sound.Jump);

            if (isGrounded)
            {
                animator.SetTrigger("Jump");
                rb.velocity = Vector2.up * jumpForce;
            }
            else if (canDoubleJump)
            {
                animator.SetTrigger("Jump");
                rb.velocity = Vector2.up * jumpForce;
                canDoubleJump = false;
            }
        }

        if (isGrounded)
        {
            canDoubleJump = true;
        }
    }

    private void FixedUpdate()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, whatIsGround);

        moveInput = Input.GetAxis("Horizontal");

        animator.SetBool("isJumping", !isGrounded ? true : false);
        animator.SetBool("isMoving", (moveInput != 0) ? true : false);


        if (canMove)
        {
            rb.velocity = new Vector2(moveInput * speed, rb.velocity.y);


            if (rb.velocity.y < 0)
            {
                rb.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
            }

            if (facingRight == false && moveInput > 0)
            {
                Flip();
            }
            else if (facingRight == true && moveInput < 0)
            {
                Flip();
            }
        }
        else
        {
            animator.SetBool("isJumping", false);
            animator.SetBool("isMoving", false);
        }
    }

    void UpdateInput()
    {
        typeButton.GetComponent<Image>().sprite = inputManager.inputActive == true ? pressed : unPressed;
        typeButton.GetComponent<Image>().color = inputManager.inputActive == true ? new Color(1, 1, 1, .4f) : new Color(1, 1, 1, 1);

        if (!PauseMenu.paused)
        {
            if (inputManager.inputActive == false)
            {
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    Invoke("ToggleInput", 0.3f);
                }
            }

            if (canPress)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    Vector2 rayPos = new Vector2(cam.ScreenToWorldPoint(Input.mousePosition).x, cam.ScreenToWorldPoint(Input.mousePosition).y);
                    RaycastHit2D hit = Physics2D.Raycast(rayPos, Vector2.zero, 0f);

                    if (hit.transform == gameObject.transform)
                    {
                        ToggleInput();
                    }
                }
            }
        }
    }

    void Flip()
    {
        facingRight = !facingRight;
        Vector3 Scaler = transform.localScale;
        Scaler.x *= -1;
        transform.localScale = Scaler;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        switch (collision.tag)
        {
            case ("Goal"):
                canPress = false;
                canMove = false;
                typeButton.SetActive(false);
                Invoke("LevelComplete", 0.5f);
                soundManager.playSound(SoundManager.Sound.Victory);
                break;
            case ("Bounds"):
                soundManager.playSound(SoundManager.Sound.Wipe);
                inputManager.ClearInput();
                gameManager.VoidOut();
                Invoke("ResetPosition", 0.45f);
                break;
            case ("Star"):
                soundManager.playSound(SoundManager.Sound.Star);
                gameManager.CollectStar(collision);
                break;
        }
    }
    private void LevelComplete()
    {
        gameManager.LevelComplete();
    }

    public void ToggleInput()
    {
        soundManager.playSound(SoundManager.Sound.Menu);
        inputManager.ToggleInputActive();
        canMove = false;
    }

    private void ResetPosition()
    {
        gameObject.transform.position = startPos;
    }
}
