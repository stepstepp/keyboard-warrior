﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialManager : MonoBehaviour
{
    public GameObject[] textBoxes;
    public int currentIndex;
    private bool hasDoneFirstInput;

    private InputManager inputManager;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 1; i < textBoxes.Length; i++)
        {
            textBoxes[i].SetActive(false);
        }

        currentIndex = 0;
        hasDoneFirstInput = false;
        inputManager = GameObject.FindGameObjectWithTag("InputManager").GetComponent<InputManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (currentIndex == 2)
        {
            if (inputManager.inputActive)
            {
                if (!hasDoneFirstInput)
                {
                    hasDoneFirstInput = false;
                    NextText();
                }
            }
        }
    }

    public void NextText()
    {
        textBoxes[currentIndex].SetActive(false);
        currentIndex++;
        textBoxes[currentIndex].SetActive(true);
    }
}
