﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Star : MonoBehaviour
{

    public Color colorIni = Color.yellow;
    public Color colorFin = Color.white;
    public float duration = 3f;
    Color lerpedColor = Color.white;

    private float t = 0;
    private bool flag;
    private float zRot;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        lerpedColor = Color.Lerp(colorIni, colorFin, t);
        gameObject.GetComponent<SpriteRenderer>().color = lerpedColor;

        if (!PauseMenu.paused)
        {
            float rZ = Mathf.SmoothStep(-15, 15, Mathf.PingPong(Time.unscaledTime, 1));
            transform.rotation = Quaternion.Euler(0, 0, rZ);
        }

        if (flag == true)
        {
            t -= Time.unscaledDeltaTime;
            if (t < 0.01f)
                flag = false;
        }
        else
        {
            t += Time.unscaledDeltaTime;
            if (t > 0.50f)
                flag = true;
        }
    }
}
