﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    private SoundManager soundManager;

    public GameObject MainScreen;
    public GameObject LevelSelectScreen;


    private void Start()
    {
        soundManager = GameObject.Find("SoundManager").GetComponent<SoundManager>();
        soundManager.playSound(SoundManager.Sound.Background);

        if (LevelSelectScreen != null)
        {
            MainScreen.SetActive(true);
            LevelSelectScreen.SetActive(false);
        }
    }

    public void NextLevel()
    {
        soundManager.playSound(SoundManager.Sound.Menu);

        string sceneName = SceneManager.GetActiveScene().name;
        int sceneNumber = int.Parse(sceneName.Substring(sceneName.Length - 1));
        sceneNumber++;

        Debug.Log(" next level is: Level " + sceneNumber);
        SceneManager.LoadScene("Level " + sceneNumber);
    }

    public void Level1()
    {
        soundManager.playSound(SoundManager.Sound.Menu);
        SceneManager.LoadScene("Level 1");
    }
    public void Level2()
    {
        soundManager.playSound(SoundManager.Sound.Menu);
        SceneManager.LoadScene("Level 2");
    }

    public void Level3()
    {
        soundManager.playSound(SoundManager.Sound.Menu);
        SceneManager.LoadScene("Level 3");
    }

    public void Level4()
    {
        soundManager.playSound(SoundManager.Sound.Menu);
        SceneManager.LoadScene("Level 4");
    }
    public void Level5()
    {
        soundManager.playSound(SoundManager.Sound.Menu);
        SceneManager.LoadScene("Level 5");
    }

    public void Level6()
    {
        soundManager.playSound(SoundManager.Sound.Menu);
        SceneManager.LoadScene("Level 6");
    }

    public void Level7()
    {
        soundManager.playSound(SoundManager.Sound.Menu);
        SceneManager.LoadScene("Level 7");
    }
    public void Level8()
    {
        soundManager.playSound(SoundManager.Sound.Menu);
        SceneManager.LoadScene("Level 8");
    }

    public void Level9()
    {
        soundManager.playSound(SoundManager.Sound.Menu);
        SceneManager.LoadScene("Level 9");
    }

    public void Level10()
    {
        soundManager.playSound(SoundManager.Sound.Menu);
        SceneManager.LoadScene("Level 10");
    }

    public void Main()
    {
        soundManager.playSound(SoundManager.Sound.Menu);
        MainScreen.SetActive(true);
        LevelSelectScreen.SetActive(false);
    }

    public void LevelSelect()
    {
        soundManager.playSound(SoundManager.Sound.Menu);
        MainScreen.SetActive(false);
        LevelSelectScreen.SetActive(true);
    }

    public void ReturnToMenu()
    {
        soundManager.playSound(SoundManager.Sound.Menu);
        SceneManager.LoadScene("Main Menu");
    }

    public void Quit()
    {
        PlayerPrefs.DeleteAll();
        soundManager.playSound(SoundManager.Sound.Menu);
        Application.Quit();
    }
}
