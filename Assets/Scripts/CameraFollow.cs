﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;
    public Vector3 offset;
    public Vector3 farCam;

    [Range(1, 10)]
    public float smoothFactor;
    private Camera cam;

    public bool followCam;

    private Vector3 origin;
    private Vector3 difference;
    private bool Drag = false;

    private void Start()
    {
        followCam = true;
        cam = gameObject.GetComponent<Camera>();
    }

    private void FixedUpdate()
    {
        if (followCam)
        {
            Vector3 targetPosition = target.position + offset;
            Vector3 smoothPosition = Vector3.Lerp(transform.position, targetPosition, smoothFactor * Time.fixedDeltaTime);
            transform.position = smoothPosition;
            gameObject.GetComponent<Camera>().orthographicSize = 9;
        }
        else
        {
            cam.orthographicSize = 13;

            if (Input.GetMouseButton(1))
            {
                difference = (cam.ScreenToWorldPoint(Input.mousePosition)) - cam.transform.position;
                if (Drag == false)
                {
                    Drag = true;
                    origin = cam.ScreenToWorldPoint(Input.mousePosition);
                }
            }
            else
            {
                Drag = false;
            }

            if (Drag == true)
            {
                cam.transform.position = ClampCamera(origin - difference);
            }
        }
    }

    Vector3 ClampCamera(Vector3 targetPosition)
    {
        float targX = target.position.x;
        float targY = target.position.y;

        return new Vector3(Mathf.Clamp(targetPosition.x, targX - 20, targX + 20), Mathf.Clamp(targetPosition.y, targY - 11, targY + 11), offset.z);
    }
}
